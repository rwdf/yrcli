package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

const cf = "./weather.cache"

func main() {
	var s string

	coords := Coordinates{
		Name: []CoordName{{
			Coord: Coord{
				Lat: defLat,
				Lon: defLon,
			},
		}},
	}

	if len(os.Args) >= 2 {
		s = os.Args[1]
		coords = GetCoordinates(s)
	}

	// check cache first
	// TODO: check cache for weather for coords
	w, valid, err := checkCache()
	if err != nil {
		log.Print(err)
	}
	if valid {
		log.Println("cached weather")
		fmt.Printf("weather: %+v", w)
		os.Exit(0)
	}

	log.Println("getting new weather from yr ...")
	w, exps, err := GetWeather(coords)
	if err != nil {
		log.Fatal(err)
	}

	err = CacheWeather(w, exps)
	if err != nil {
		log.Fatal(err)
	} else {
		log.Println("caching weather")
	}
}

func checkCache() (Weather, bool, error) {
	var wc WeatherCache

	wb, err := os.ReadFile(cf)
	if err != nil {
		return wc.Weather, false, fmt.Errorf("couldn't read cache file: %s", err)
	}

	// if cache file exists, parse it to check the expiry time
	err = json.Unmarshal(wb, &wc)
	if err != nil {
		return wc.Weather, false, fmt.Errorf("couldn't parse cache file: %s", err)
	}
	// expiry date is a string that looks like this
	// Sat, 15 Jun 2024 11:45:53 GMT
	expTime, err := time.Parse(time.RFC1123, wc.Expiry)
	if err != nil {
		return wc.Weather, false, err
	}
	if expTime.After(time.Now()) {
		return wc.Weather, false, nil
	}

	wc.Weather, err = ParseWeather(wb)
	if err != nil {
		return wc.Weather, false, err
	}
	return wc.Weather, true, nil
}

func CacheWeather(w Weather, exp string) error {
	var wc WeatherCache
	wc.Expiry = exp
	wc.Weather = w

	wb, err := json.Marshal(wc)
	if err != nil {
		return fmt.Errorf("failed to convert weather to json: %s", err.Error())
	}
	err = os.WriteFile(cf, []byte(wb), 0664)
	if err != nil {
		return fmt.Errorf("failed to write cache file: %s", err.Error())
	}

	return nil
}

func GetCoordinates(s string) Coordinates {
	res, err := http.Get("https://api.kartverket.no/stedsnavn/v1/navn?sok=" + s + "&utkoordsys=4258&treffPerSide=10&side=1&filtrer=navn%2Cnavn.representasjonspunkt")
	if err != nil {
		log.Fatal("Failed to get coordinates")
	}

	defer res.Body.Close()

	if res.StatusCode != 200 {
		log.Fatalf("Error code from coordinate API: %v", res.StatusCode)
	}

	// TODO: Don't use ReadAll here? Json Unmarshal into struct of response.
	body, err := io.ReadAll(res.Body)
	if err != nil {
		log.Fatalf("Couldn't read response body from coordinate API: %s", err)
	}

	var coords Coordinates
	err = json.Unmarshal(body, &coords)
	if err != nil {
		log.Fatalf("Couldn't get coordinates from response: %s", err)
	}
	return coords
}

func GetWeather(c Coordinates) (Weather, string, error) {
	var w Weather

	lat := strconv.FormatFloat(c.Name[0].Coord.Lat, 'g', -1, 64)
	lon := strconv.FormatFloat(c.Name[0].Coord.Lon, 'g', -1, 64)

	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://api.met.no/weatherapi/locationforecast/2.0/compact?lat="+lat+"&lon="+lon, nil)
	if err != nil {
		return w, "", fmt.Errorf("couldn't build request for Yr API %s", err)
	}

	req.Header.Set("User-Agent", sitename)

	res, err := client.Do(req)
	if err != nil {
		return w, "", fmt.Errorf("call to Yr API failed: %s", err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return w, "", fmt.Errorf("couldn't read response body from Yr API: %s", err)
	}

	// TODO: move to func ParseWeather()
	w, err = ParseWeather(body)
	if err != nil {
		return w, "", err
	}

	expiry := res.Header.Get("expires")

	fmt.Printf("weather: %+v", w.Properties.Timeseries[0].Data.Instant.Details)
	return w, expiry, nil
}

func ParseWeather(body []byte) (Weather, error) {
	var w Weather
	err := json.Unmarshal(body, &w)
	if err != nil {
		return w, fmt.Errorf("failed to parse weather from yr: %s", err.Error())
	}
	return w, nil
}
