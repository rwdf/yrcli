# Yr CLI

## TODO:

- Extract the useful information from the response from Yr
    - present the current weather plus the forecast for the day
    - make a summary of the next X days
- Don't get new weather if last forecast is not expired yet
- Store the last forecast ...
    - in a sqlite db: fields for place/coords, time/lastchanged and the JSON
- Take a toml config file with default location
- Check the cached results for coords as well as expiry

## WIP

- the cache stuff 

